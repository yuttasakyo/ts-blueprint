import './adapters/amqp';
import { config } from './bootstrapConfig';
import { defaultLogger } from './bootstrapLogger';
import { traceUtil } from './bootstrapTracer';
import { createAmqpConnection, IAmqpConnectionOptions } from './libraries/amqp';
import { gracefulShutdown } from './libraries/shutdown/gracefulShutdown';
import { ensureConfigKeys } from './utils/configUtil';

async function bootstrapAmqp() {
  try {
    ensureConfigKeys(config.amqp, 'AMQP_URI')

    const options: IAmqpConnectionOptions = {
      connectionUrl: config.amqp.AMQP_URI!,
      username: config.amqp.AMQP_USERNAME,
      password: config.amqp.AMQP_PASSWORD,
      prefix: config.amqp.AMQP_PREFIX,
    }

    const connection = await createAmqpConnection(options, defaultLogger, traceUtil);

    defaultLogger.info({ event: 'amqp_connected' }, `Successfully connect to amqp server`);

    gracefulShutdown(defaultLogger, 'amqp', ['http'], async () => {
      await connection.close()
    });

  } catch (error) {
    defaultLogger.error(error, { event: 'bootstrap_amqp' })

    process.exit(-1)
  }
}

bootstrapAmqp();