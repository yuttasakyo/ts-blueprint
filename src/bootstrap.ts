// !!!Important note!!!
// Import sequence in this file matters
import "reflect-metadata"; // enable dependency injection using type di

// boot essentials first
import './bootstrapConfig'
import './bootstrapTracer'
import './bootstrapLogger'

// boot repository
// import './bootstrapMongo';
import './bootstrapInMemory'; // comment this line and uncomment above line to use real mongo as a database

// boot application
// import './bootstrapAuthentication'
// import './bootstrapAuthorization'
import './bootstrap​RestApi';
// import './bootstrapAmqp';
