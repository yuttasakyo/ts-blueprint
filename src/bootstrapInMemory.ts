import './adapters/mem/repositories'; // register all mongo repositories
import { defaultLogger } from './bootstrapLogger';

defaultLogger.info({ event: 'bootstrap_inmemory_repo' })