import { findWithPagination } from './pagination'

const userModel = (num: number) => {
  let users = []
  for (let i = 0; i < num; i++) {
    users.push({
      _id: i,
      name: 'test'
    })
  }
  return users
}

describe('Pagination', () => {
  beforeEach(() => jest.clearAllMocks())

  it('should call nested functions and return documents', async () => {
    const model: any = {
      find: jest.fn(() => model),
      sort: jest.fn(() => model),
      skip: jest.fn(() => model),
      limit: jest.fn(() => model),
      countDocuments: jest.fn(() => 1000),
      exec: jest.fn(() => userModel(10))
    }

    const result = await findWithPagination(
      model,
      { occupation: /host/,
        'name.last': 'Ghost',
        age: { $gt: 17, $lt: 66 },
        likes: { $in: ['vaporizing', 'talking'] }
      },
      { page: 3, limit: 10, sort: { createdAt: -1 } })

    expect(model.find).toHaveBeenCalledTimes(1)
    expect(model.find).toHaveBeenCalledWith({
      occupation: /host/,
      'name.last': 'Ghost',
      age: { $gt: 17, $lt: 66 },
      likes: { $in: ['vaporizing', 'talking'] }
    })

    expect(model.sort).toHaveBeenCalledTimes(1)
    expect(model.sort).toHaveBeenCalledWith({ createdAt: -1 })
    expect(model.limit).toHaveBeenCalledTimes(1)
    expect(model.limit).toHaveBeenCalledWith(10)
    expect(model.skip).toHaveBeenCalledTimes(1)
    expect(model.skip).toHaveBeenCalledWith(20)
    expect(model.countDocuments).toHaveBeenCalledTimes(1)
    expect(model.exec).toHaveBeenCalledTimes(1)
    expect(result).toBeTruthy()
    expect(result).toHaveProperty('documents')
    expect(result.documents).toHaveLength(10)
    expect(result).toHaveProperty('pageable')
    expect(result.pageable).toEqual({
      page: 3,
      pageSize: 10,
      count: 10,
      total: 1000,
      totalPages: 100,
      hasNext: true
    })
  })

  it('should return documents with default limit when does not send limit or equal 0 to param', async () => {
    const model: any = {
      find: jest.fn(() => model),
      sort: jest.fn(() => model),
      skip: jest.fn(() => model),
      limit: jest.fn(() => model),
      countDocuments: jest.fn(() => 100),
      exec: jest.fn(() => userModel(25))
    }

    const result = await findWithPagination(
      model,
      {},
      { page: 1, limit: 0, sort: { createdAt: -1 } })

    expect(model.sort).toHaveBeenCalledTimes(1)
    expect(model.limit).toHaveBeenCalledTimes(1)
    expect(model.limit).toHaveBeenCalledWith(25)
    expect(model.skip).toHaveBeenCalledTimes(1)
    expect(model.skip).toHaveBeenCalledWith(0)
    expect(result).toBeTruthy()
    expect(result).toHaveProperty('documents')
    expect(result.documents).toHaveLength(25)
    expect(result).toHaveProperty('pageable')
    expect(result.pageable).toEqual({
      page: 1,
      pageSize: 25,
      count: 25,
      total: 100,
      totalPages: 4,
      hasNext: true
    })
  })

  it('should return documents and hasNext is false', async () => {
    const model: any = {
      find: jest.fn(() => model),
      sort: jest.fn(() => model),
      skip: jest.fn(() => model),
      limit: jest.fn(() => model),
      countDocuments: jest.fn(() => 200),
      exec: jest.fn(() => userModel(20))
    }

    const result = await findWithPagination(
      model,
      {},
      { page: 10, limit: 20 })

    expect(model.sort).toHaveBeenCalledTimes(1)
    expect(result).toBeTruthy()
    expect(result).toHaveProperty('documents')
    expect(result.documents).toHaveLength(20)
    expect(result).toHaveProperty('pageable')
    expect(result.pageable).toEqual({
      page: 10,
      pageSize: 20,
      count: 20,
      total: 200,
      totalPages: 10,
      hasNext: false
    })
  })
})
