import { ValidationError } from 'class-validator';

export function formatClassValidatorErrors(errors: ValidationError[]) {
  if (errors && errors.length > 0) {
    const formattedErrors: string[] = [];
    for (const err of errors) {
      if (err.constraints) {
        const messages = Object.values(err.constraints);
        formattedErrors.push(...messages);
      }
    }
    return formattedErrors;
  }

  return
}
