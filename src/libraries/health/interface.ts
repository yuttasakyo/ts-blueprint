export interface IHealth {
  ok: boolean
  status?: string
  statusText?: string
  details?: {
    [key: string]: IHealth
  }
}

export interface IHealthCheck {
  getName(): string
  getHealth(): Promise<IHealth>
}