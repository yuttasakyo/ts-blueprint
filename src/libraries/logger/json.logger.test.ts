import { JsonLogger } from "./json.logger"

class StreamCatcher {
    logs: any[] = []

    write(log: any) {
        this.logs.push(log)
    }

    clear() {
        this.logs = []
    }
}

function assertLogAvailable(catcher: StreamCatcher) {
    const logs = catcher.logs
    expect(logs).toHaveLength(1)

    return logs[0]
}

function assertLogMsg(catcher: StreamCatcher, expectedMsg: string, level?: number) {
    const log = assertLogAvailable(catcher)

    expect(log.msg).toBe(expectedMsg)

    if (level) {
        expect(log.level).toBe(level)
    }

    return log
}

describe('general logs', () => {
    let logger: JsonLogger
    let catcher: StreamCatcher

    beforeAll(() => {
        catcher = new StreamCatcher()

        logger = new JsonLogger('test', {
            lastRawStream: catcher as any,
        })
    });

    beforeEach(() => {
        catcher.clear()
    })

    test('should log a msg', () => {
        logger.info('test')
        assertLogMsg(catcher, 'test')
    });

    test('should log an error when put error as 1st param', () => {
        logger.error(new Error('test error'), 'test')
        const log = assertLogMsg(catcher, 'test', 50)
        expect(log.err).toBeDefined()
    });

    test('should append error to msg', () => {
        logger.error('test', new Error('test error'))
        const log = assertLogAvailable(catcher)
        expect(log.err).not.toBeDefined()
    });
})

describe('logs types', () => {
    let logger: JsonLogger
    let catcher: StreamCatcher

    beforeAll(() => {
        catcher = new StreamCatcher()

        logger = new JsonLogger('test', {
            lastRawStream: catcher as any,
            level: 'trace'
        })
    });

    beforeEach(() => {
        catcher.clear()
    })

    test('should log info normally', () => {
        logger.info('test')
        assertLogMsg(catcher, 'test', 30)
    });

    test('should log warn normally', () => {
        logger.warn('test')
        assertLogMsg(catcher, 'test', 40)
    });

    test('should log debug normally', () => {
        logger.debug('test')
        assertLogMsg(catcher, 'test', 20)
    });

    test('should log error normally', () => {
        logger.error('test')
        assertLogMsg(catcher, 'test', 50)
    });

    test('should log trace normally', () => {
        logger.trace('test')
        assertLogMsg(catcher, 'test', 10)
    });

})

describe('log level', () => {
    let catcher: StreamCatcher

    beforeAll(() => {
        catcher = new StreamCatcher()
    });

    beforeEach(() => {
        catcher.clear()
    })

    test('should not log debug level', () => {
        const logger = new JsonLogger('test', {
            lastRawStream: catcher as any,
        })
        logger.debug('test')
        expect(catcher.logs).toHaveLength(0)
    });

    test('should log debug level', () => {
        const logger = new JsonLogger('test', {
            lastRawStream: catcher as any,
            level: 'debug'
        })
        logger.debug('test')
        expect(catcher.logs).toHaveLength(1)
    });
})

describe('tracing logs', () => {
    const catcher: StreamCatcher = new StreamCatcher()

    const tracedLogger = new JsonLogger('test', {
        lastRawStream: catcher as any,
    });

    tracedLogger.tracer = {
        getLocalTracingKey: () => 'traceKey',
        getLocalTracingId: () => 'traceId1'
    };

    beforeEach(() => {
        catcher.clear()
    });

    test('should log trace data', () => {
        tracedLogger.info('test')

        let log = assertLogAvailable(catcher)
        expect(log.traceKey).toBe('traceId1')
        expect(log.msg).toBe('test')
    });

    test('should log trace data when log obj', () => {
        tracedLogger.info({ test: '1' }, '2')

        let log = assertLogAvailable(catcher)
        expect(log.traceKey).toBe('traceId1')
        expect(log.test).toBe('1')
        expect(log.msg).toBe('2')
    });

    test('should log trace data when log error', () => {
        const err = new Error('test error')
        tracedLogger.info(err, '1')

        let log = assertLogAvailable(catcher)
        expect(log.traceKey).toBe('traceId1')
        expect(log.err).toBeDefined()
        expect(log.err.message).toBe('test error')
        expect(log.msg).toBe('1')
    });

    test('should log trace data when log other', () => {
        tracedLogger.info(1)

        let log = assertLogAvailable(catcher)
        expect(log.traceKey).toBe('traceId1')
        expect(log.msg).toBe('1')
    });

    test('should log correct trace value when trace value changed', () => {
        tracedLogger.info('test')

        let log = assertLogAvailable(catcher)
        expect(log.traceKey).toBe('traceId1')
        expect(log.msg).toBe('test')

        catcher.clear()

        tracedLogger.info('test2')

        log = assertLogAvailable(catcher)
        expect(log.traceKey).toBe('traceId1')
        expect(log.msg).toBe('test2')
    });

    test('should not log trace key', () => {
        const logger = new JsonLogger('test', {
            lastRawStream: catcher as any,
        })

        logger.info('test')
        const log = assertLogAvailable(catcher)
        expect(log.msg).toBe('test')
        expect(log.traceKey).not.toBeDefined()
    });

})
