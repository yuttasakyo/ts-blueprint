import { Options } from 'amqplib';
import { BrokerConfig } from 'rascal';
import { IAmqpConnectionOptions } from '.';
import { ErrorHandlingStrategy, IConsumerMeta, IPublisherMeta } from './type';

export interface IExchangeConfig {
  assert?: boolean;
  check?: boolean;
  type?: 'direct' | 'fanout' | 'headers' | 'topic';
  options?: Options.AssertExchange;
}

export interface IPublicationConfigBase {
  vhost?: string;
  exchange?: string;
  queue?: string;
  routingKey?: string;
  confirm?: boolean;
  options?: Options.Publish;
  autoCreated?: boolean;
  deprecated?: boolean;
  encryption?: string;
}

export interface IPublicationConfig extends IPublicationConfigBase {
  exchangeConfig?: IExchangeConfig
}

export interface IRetryConfig {
  factor?: number;
  max?: number;
  min?: number;
  strategy?: 'exponential' | 'linear';
  delay?: number;
}

export interface ISubscriptionConfigBase {
  vhost?: string;
  queue?: string;
  contentType?: string;
  options?: Options.Consume;
  prefetch?: number;
  retry?: IRetryConfig | boolean;
  handler?: string;
  handlers?: string[];
  recovery?: any;
  deferCloseChannel?: number;
  encryption?: string;
  autoCreated?: boolean;
  redeliveries?: {
    counter: string;
    limit: number;
    timeout?: number;
  };
}

export interface ISubscriptionConfig extends ISubscriptionConfigBase {
  errorStrategy?: ErrorHandlingStrategy
}

export interface IBrokerConfig extends IAmqpConnectionOptions {
  consumerMetas?: IConsumerMeta[]
  publisherMetas?: IPublisherMeta[]
}

export interface IQueueConfigBase {
  assert?: boolean;
  check?: boolean;
  options?: Options.AssertQueue;
}

export interface IQueueConfig extends IQueueConfigBase {
  queueName: string;
  exchange?: string;
  exchangeConfig?: IExchangeConfig
  routingKey?: string

  disabled?: boolean
}

export interface IBindingConfig {
  source?: string;
  destination?: string;
  destinationType?: 'queue' | 'exchange';
  bindingKey?: string;
  bindingKeys?: string[];
  options?: any;
}

export function buildConfig(config: IBrokerConfig): BrokerConfig {
  return {
    vhosts: {
      config: {
        connections: buildConnections(config) as any,
        exchanges: buildExchanges(config),
        queues: buildQueues(config),
        bindings: buildBindings(config),
        subscriptions: buildSubscriptions(config),
        publications: buildPublications(config),
      }
    }
  }
}

function buildSubscriptions(config: IBrokerConfig): { [key: string]: ISubscriptionConfigBase } {
  const subscriptions: { [key: string]: ISubscriptionConfigBase } = {};

  // add exchange for consumers
  if (config.consumerMetas) {
    for (const meta of config.consumerMetas) {
      const name = meta.queueConfig.queueName;
      subscriptions[name] = buildSubscription(meta, config);
    }
  }

  return subscriptions;
}

function buildSubscription(meta: IConsumerMeta, config: IBrokerConfig): ISubscriptionConfigBase {
  const subConfig = meta.subConfig || { queue: '' };

  // remove custom config
  const { errorStrategy, ...baseConfig } = subConfig;

  baseConfig.queue = getQueueName(meta.queueConfig, config);

  return Object.assign({}, config.defaultSubscription, baseConfig);
}

function buildPublications(config: IBrokerConfig): { [key: string]: IPublicationConfigBase } {
  const publications: { [key: string]: IPublicationConfigBase } = {};

  // add queue for publisher
  if (config.publisherMetas) {
    for (const meta of config.publisherMetas) {
      const { exchange, queue } = meta.pubConfig;
      const name = exchange || queue || '/';

      publications[name] = buildPublication(meta, config);
    }
  }

  return publications
}

function buildPublication(meta: IPublisherMeta, config: IBrokerConfig): IPublicationConfigBase {
  // remove custom config
  const { exchangeConfig, ...baseConfig } = meta.pubConfig;

  if (baseConfig.exchange) {
    baseConfig.exchange = getPublicationExchangeName(baseConfig, config);
  }

  if (baseConfig.queue) {
    baseConfig.queue = getPublicationQueueName(baseConfig, config);
  }

  return baseConfig;
}

function getQueueName(config: IQueueConfig, brokerConfig: IBrokerConfig) {
  const { queueName } = config;

  return addPrefix(queueName, brokerConfig);
}

function getPublicationQueueName(config: IPublicationConfig, brokerConfig: IBrokerConfig) {
  const { queue } = config;

  return addPrefix(queue!, brokerConfig);
}

function addPrefix(name: string, brokerConfig: IBrokerConfig) {
  const { prefix = '' } = brokerConfig;

  return `${prefix}${name}`;
}

function buildQueues(config: IBrokerConfig): { [key: string]: IQueueConfigBase } {
  const queues: { [key: string]: IQueueConfigBase } = {};

  // add queue for publisher
  if (config.publisherMetas) {
    for (const meta of config.publisherMetas) {
      if (meta.pubConfig.queue) {
        const name = getPublicationQueueName(meta.pubConfig, config);
        queues[name] = {}
      }
    }
  }

  // add queue for consumers
  if (config.consumerMetas) {
    for (const meta of config.consumerMetas) {
      const name = getQueueName(meta.queueConfig, config);
      queues[name] = buildQueue(meta, config);
    }
  }

  return queues;
}

function buildQueue(meta: IConsumerMeta, brokerConfig: IBrokerConfig): IQueueConfigBase {
  const config = meta.queueConfig;

  // remove custom config
  const { queueName, routingKey, exchange, exchangeConfig, disabled, ...baseConfig } = config;

  // add prefix to dead letter
  if (config.options) {
    const { deadLetterExchange, arguments: optArgs } = config.options;

    if (deadLetterExchange) {
      config.options.deadLetterExchange = addPrefix(deadLetterExchange, brokerConfig);
    } else if (optArgs && optArgs['x-dead-letter-exchange']) {
      config.options.arguments['x-dead-letter-exchange'] = addPrefix(optArgs['x-dead-letter-exchange'], brokerConfig);
    }
  }

  return baseConfig;
}

function buildBindings(config: IBrokerConfig): { [key: string]: IBindingConfig } {
  const bindings: { [key: string]: IBindingConfig } = {};

  if (!config.consumerMetas) return bindings;

  for (const meta of config.consumerMetas) {
    const { exchange, queueName, routingKey } = meta.queueConfig;

    if (!exchange) continue;

    const bindingName = `${exchange}-${queueName}`;
    bindings[bindingName] = {
      source: getQueueExchangeName(meta.queueConfig, config),
      destination: getQueueName(meta.queueConfig, config),
      bindingKey: routingKey,
    }
  }

  return bindings;
}

function getQueueExchangeName(config: IQueueConfig, brokerConfig: IBrokerConfig) {
  const { exchange } = config;

  return addPrefix(exchange!, brokerConfig);
}

function getPublicationExchangeName(config: IPublicationConfig, brokerConfig: IBrokerConfig) {
  const { exchange } = config;

  return addPrefix(exchange!, brokerConfig);
}

function buildExchanges(config: IBrokerConfig): { [key: string]: IExchangeConfig } {
  const exchanges: { [key: string]: IExchangeConfig } = {};

  // add exchange for consumers
  if (config.consumerMetas) {
    for (const meta of config.consumerMetas) {
      if (meta.queueConfig.exchange) {
        const name = getQueueExchangeName(meta.queueConfig, config);
        exchanges[name] = meta.queueConfig.exchangeConfig || {}
      }
    }
  }

  // add exchange for publisher
  if (config.publisherMetas) {
    for (const meta of config.publisherMetas) {
      if (meta.pubConfig.exchange) {
        const name = getPublicationExchangeName(meta.pubConfig, config);
        exchanges[name] = meta.pubConfig.exchangeConfig || {}
      }
    }
  }

  return exchanges;
}

function buildConnections(config: IBrokerConfig) {
  const { connectionUrl, username, password } = config;

  const connections = connectionUrl.split(',');

  if (username && password) {
    return connections.map(connection => {
      return connection.replace('://', `://${username}:${password}@`);
    })
  }

  return connections;
}