import { Message } from 'amqplib';
import { InvalidMessage } from './errorType';
import { ErrorHandlingStrategy, FORWARD, REJECT, REJECT_WITH_ERROR_INFO, REPUBLISH, REQUEUE } from './type';

const DEFAULT_DEFER = 1000;

export function handleOnMessageError(
  err: any,
  ackOrNack: any,
  message: Message,
  strategy: ErrorHandlingStrategy = {
    type: REPUBLISH,
    attempts: 10
  }) {
  if (err instanceof InvalidMessage) {
    return disrecardOrRouteToDLWithErrorInfo(err, ackOrNack, message);
  }

  switch (strategy.type) {
    case FORWARD:
      return forward(err, ackOrNack, strategy.exchange, strategy.attempts);
    case REPUBLISH:
      return republish(err, ackOrNack, strategy.attempts);
    case REQUEUE:
      return requeue(err, ackOrNack);
    case REJECT:
      return disrecardOrRouteToDL(err, ackOrNack);
    case REJECT_WITH_ERROR_INFO:
      return disrecardOrRouteToDLWithErrorInfo(err, ackOrNack, message);
    default:
      return disrecardOrRouteToDLWithErrorInfo(err, ackOrNack, message);
  }

}

function forward(err: any, ackOrNack: any, exchange: string, attempts?: number) {
  if (attempts) {
    ackOrNack(err, [
      { strategy: 'forward', publication: exchange, defer: DEFAULT_DEFER, attempts },
      { strategy: 'nack' }
    ])
  } else {
    ackOrNack(err, { strategy: 'forward', publication: exchange });
  }
}

function disrecardOrRouteToDL(err: any, ackOrNack: any) {
  ackOrNack(err, { strategy: 'nack' });
}

function disrecardOrRouteToDLWithErrorInfo(err: any, ackOrNack: any, _message: Message, ) {
  ackOrNack(err, { strategy: 'republish', immediateNack: true })
}

function requeue(err: any, ackOrNack: any) {
  ackOrNack(err, { strategy: 'nack', defer: DEFAULT_DEFER, requeue: true });
}

function republish(err: any, ackOrNack: any, attempts?: number) {
  if (attempts) {
    ackOrNack(err, [
      { strategy: 'republish', defer: DEFAULT_DEFER, attempts },
      { strategy: 'nack' }
    ])
  } else {
    ackOrNack(err, { strategy: 'republish', defer: DEFAULT_DEFER })
  }
}