import { MetaDataRegistry } from './metaDataRegistry';
import { IPublicationConfig, IQueueConfig, ISubscriptionConfig } from './rascalConfig';
import { IBroker, IConsumerFactory, IPublicationMessage, MessageCallback } from './type';
import { invokePromiseOrFunction } from './util';

// TODO add unit test

export class AmqpConnection {
  static connection?: AmqpConnection;

  constructor(private broker: IBroker) {
    AmqpConnection.connection = this;
  };

  static async PUBLISH(pubConfig: IPublicationConfig, resultFn: Function, args: any[], context: any) {
    let result = await invokePromiseOrFunction(resultFn, context, args);
    AmqpConnection.connection?.publish(pubConfig, result);
    return result;
  }

  async subscribeAllConsumers(consumerFactory: IConsumerFactory) {
    for (const consumerMeta of MetaDataRegistry.consumerMetas) {
      if (consumerMeta.queueConfig.disabled) continue;

      const service = consumerFactory.get(consumerMeta);

      const { queueConfig, propertyKey, subConfig } = consumerMeta;

      await this.subscribe(queueConfig, service[propertyKey], service, subConfig);
    }
  }

  subscribe(queueConfig: IQueueConfig, cb: MessageCallback, context?: any, subConfig?: ISubscriptionConfig) {
    return this.broker.subscribe(queueConfig, cb, context, subConfig);
  }

  publish(pubConfig: IPublicationConfig, message: IPublicationMessage) {
    return this.broker.publish(pubConfig, message);
  }

  async close() {
    await this.broker.shutdown();
  }
}