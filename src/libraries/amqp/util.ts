export async function invokePromiseOrFunction(fn: Function, context: any, args: any[]) {
  const result = fn.apply(context, args);

  if ('then' in result) {
    return await result;
  }

  return result;
}