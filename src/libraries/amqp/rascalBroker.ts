import { Message } from 'amqplib';
import { EventEmitter } from 'events';
import { Span } from 'opentracing';
import { BrokerAsPromised as Broker, BrokerConfig } from 'rascal';
import { ILogger } from '../logger/logger.interface';
import { TraceUtil } from '../tracing/traceUtil';
import { handleOnMessageError } from './errorHandler';
import { InvalidMessage } from './errorType';
import { IPublicationConfig, IQueueConfig, ISubscriptionConfig } from './rascalConfig';
import { IBroker, MessageCallback, PublicationMessage } from './type';
import { invokePromiseOrFunction } from './util';

interface IPublishEventemitter extends EventEmitter {
  on(event: 'success', listener: (messageId: string) => void): this;
  on(event: 'error', listener: (err: Error, messageId: string) => void): this;
  on(event: 'return', listener: (message: Message) => void): this;
}

interface ISubscriptionSession extends EventEmitter {
  name: string;
  isCancelled(): boolean;
  cancel(): Promise<void>;
  on(event: 'message', listener: (message: Message, content: any, ackOrNackFn: any) => void): this;
  on(event: 'error' | 'cancelled', listener: (err: Error) => void): this;
  on(event: 'invalid_content' | 'redeliveries_exceeded' | 'redeliveries_error' | 'redeliveries_error', listener: (err: Error, message: Message, ackOrNackFn: any) => void): this;
}

interface IRascalBroker extends EventEmitter {
  readonly config: BrokerConfig;
  connect(name: string): Promise<any>;
  nuke(): Promise<void>;
  purge(): Promise<void>;
  shutdown(): Promise<void>;
  bounce(): Promise<void>;
  unsubscribeAll(): Promise<void>;
  publish(name: string, message: any, overrides?: IPublicationConfig | string): Promise<IPublishEventemitter>;
  forward(name: string, message: any, overrides?: IPublicationConfig | string): Promise<IPublishEventemitter>;
  subscribe(name: string, overrides?: ISubscriptionConfig): Promise<ISubscriptionSession>;
}

export class RascalBroker implements IBroker {
  private broker!: IRascalBroker;

  constructor(
    private brokerConfig: BrokerConfig,
    private logger: ILogger,
    private tracer?: TraceUtil) { }

  async connect() {
    this.broker = await Broker.create(this.brokerConfig);
    this.broker.on('error', (err: any) => {
      this.logger.error(err, { event: 'rascal_error', error_type: 'broker_config' });
    });

    return this.broker;
  }

  private injectTracingContext(config: IPublicationConfig) {
    if (this.tracer) {
      // inject tracing context
      config.options = config.options || {};
      config.options.headers = config.options.headers || {};
      this.tracer.injectTracingContext(config.options.headers);
    }
  }

  private startSpan(queueConfig: IQueueConfig, message: Message) {
    if (!this.tracer) return;

    return this.tracer.startSpanFromHeader(
      `amqp: ${queueConfig.queueName}`,
      message.properties.headers,
      {
        type: 'amqp', tags: {
          queue: queueConfig.queueName,
          exchange: queueConfig.exchange || '',
          routingKey: queueConfig.routingKey || '',
        }
      })
  }

  private finishSpan(span?: Span, err?: any) {
    if (!span || !this.tracer) return;

    this.tracer.finishSpan(span, err);
  }

  async publish(config: IPublicationConfig, message: PublicationMessage) {
    let content = message;

    if ('message' in message) {
      content = message.message;
      config = Object.assign({}, config, message.config);
    }

    this.injectTracingContext(config);

    const name = config.exchange || config.queue || '/';

    const publication = await this.broker.publish(name, content, config);
    publication.on('error', (err: any) => {
      this.logger.error(err, { event: 'rascal_error', error_type: 'publish_config', exchange: config.exchange, queue: config.queue });
    });
  }

  async subscribe(queueConfig: IQueueConfig, cb: MessageCallback, context?: any, config?: ISubscriptionConfig) {
    const subscription = await this.broker.subscribe(queueConfig.queueName);
    subscription.on('message', async (message: Message, content: any, ackOrNack: any) => {
      const span = this.startSpan(queueConfig, message);

      try {
        await invokePromiseOrFunction(cb, context, [content]);
        ackOrNack();

        this.finishSpan(span);
      } catch (err) {
        handleOnMessageError(err, ackOrNack, message, config?.errorStrategy);

        this.finishSpan(span, err);
      }
    })

    subscription.on('invalid_content', (err: any, message: Message, ackOrNack: any) => {
      this.logger.error(err, { event: 'rascal_error', error_type: 'invalid_content', queue: queueConfig.queueName });
      handleOnMessageError(new InvalidMessage(err), ackOrNack, message);
    })

    subscription.on('error', (err: any) => {
      this.logger.error(err, { event: 'rascal_error', error_type: 'subscribe_config', queue: queueConfig.queueName });
    });
  }

  shutdown() {
    return this.broker.shutdown();
  }

}