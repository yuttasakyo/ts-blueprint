import axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios';

export interface IRestClientLogger {
  error(obj: Object, ...params: any[]): void;
  info(obj: Object, ...params: any[]): void;
}

export interface IConfig extends AxiosRequestConfig {
  notFoundAsNull?: boolean;
}

export interface IRequest extends AxiosRequestConfig {
}

export interface IRestClient {
  request<ResData>(request: IRequest): Promise<ResData | null>;
  get<ResData>(url: string, request: IRequest): Promise<ResData | null>;
  post<ResData>(url: string, request: IRequest): Promise<ResData | null>;
  put<ResData>(url: string, request: IRequest): Promise<ResData | null>;
  delete<ResData>(url: string, request: IRequest): Promise<ResData | null>;
}

export type Params = { [key: string]: string | number | boolean };

export class ResponseError extends Error {
  status: number;
  statusText?: string;
  data?: any;
  headers?: any;

  constructor(
    status: number,
    statusText?: string,
    headers?: any,
    data?: any,
  ) {
    super(`${status} ${statusText}`);
    this.status = status;
    this.statusText = statusText;
    this.headers = headers;
    this.data = data;
  }
}

export class RequestError extends Error {
  constructor(message: string) {
    super(message);
  }
}

function handleError(error: any, config: IConfig = {}, _logger: IRestClientLogger) {
  if (error.response) {
    if (error.response.status === 404 && config.notFoundAsNull === true) {
      return Promise.resolve(null);
    }

    // The request was made and the server responded with a status code
    // that falls out of the range of 2xx
    const { status, statusText, headers, data } = error.response;
    return Promise.reject(
      new ResponseError(status, statusText, headers, data),
    );
  } else if (error.request) {
    // The request was made but no response was received
    // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
    // http.ClientRequest in node.js
    return Promise.reject(new RequestError(error));
  }

  return Promise.reject(error);
}

function handleResponse<ResData>(
  response: AxiosResponse<ResData>,
  _logger: IRestClientLogger,
) {
  return response.data;
}

export class NoopLogger implements IRestClientLogger {
  info(_obj: any, ..._params: any[]) { }
  error(_obj: Object, ..._params: any[]) { }
}

export class RestClient implements IRestClient {
  private axios: AxiosInstance;
  private logger: IRestClientLogger;

  constructor(private config?: IConfig, logger?: IRestClientLogger) {
    this.axios = axios.create(config);
    this.config = config;
    this.logger = logger || new NoopLogger();
  }

  request<ResData>(request: IRequest): Promise<ResData | null> {
    return this.axios
      .request<ResData>(request)
      .then((response: AxiosResponse<ResData>) =>
        handleResponse(response, this.logger),
      )
      .catch((error: any) => handleError(error, this.config, this.logger));
  }

  get<ResData>(url: string, request: IRequest = {}) {
    return this.request<ResData>({
      method: 'GET',
      url,
      ...request,
    });
  }

  post<ResData>(url: string, request: IRequest = {}) {
    return this.request<ResData>({
      method: 'POST',
      url,
      ...request,
    });
  }

  put<ResData>(url: string, request: IRequest = {}) {
    return this.request<ResData>({
      method: 'PUT',
      url,
      ...request,
    });
  }

  delete<ResData>(url: string, request: IRequest = {}) {
    return this.request<ResData>({
      method: 'DELETE',
      url,
      ...request,
    });
  }
}
