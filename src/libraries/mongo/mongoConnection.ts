import { connect, ConnectionOptions } from 'mongoose';
import { consoleLogger } from '../../logger';
import { ILogger } from '../logger/logger.interface';
export interface IMongoConnection {
  uri: string;
  options: ConnectionOptions;
}

export class MongoConnection {
  private uri: string;
  private options: ConnectionOptions;
  private connected: boolean = false;

  constructor({ uri, options }: IMongoConnection, private logger: ILogger = consoleLogger) {
    this.uri = uri;
    this.options = options;
  }

  connect = async () => {
    if (this.connected) return;

    await connect(this.uri, this.options);
    this.logger.info({ event: 'mongo_connected', uri: this.uri, dbName: this.options.dbName }, `Successfully to connected MongoDB`)
    this.connected = true;
  };
}
