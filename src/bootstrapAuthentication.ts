import Container from 'typedi';
import { config } from './bootstrapConfig';
import { AuthenticationProvider } from './libraries/authentication/authenticationProvider';
import { IAuthenticationOptions, IUserRepository } from './libraries/authentication/type';
import { IUser } from './repositories/user/user.repository';

const options: IAuthenticationOptions = {
  allowAnonymous: false,
  useCustomHeader: true,
}

if (config.JWT_SECRET) {
  options.useJwt = true;
  options.jwtOptions = {
    secret: config.JWT_SECRET,
  }
}

const userRepo = Container.get<IUserRepository<IUser>>('IUserRepository')

export const authenticationProvider = new AuthenticationProvider<IUser>(userRepo, options);

Container.set({
  type: AuthenticationProvider,
  value: authenticationProvider
})