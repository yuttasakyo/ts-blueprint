import { Env, Load } from '@tel/env-decorator';
import * as dotenv from 'dotenv';
import { ILogLevel } from './libraries/logger/json.logger';

dotenv.config();

class MongoConfig {
  @Env(['MONGO_URI'])
  MONGO_URI: string | undefined;

  @Env(['MONGO_USERNAME'])
  MONGO_USERNAME: string | undefined;

  @Env(['MONGO_PASSWORD'])
  MONGO_PASSWORD: string | undefined;

  @Env(['MONGO_DATABASE_NAME'])
  MONGO_DATABASE_NAME: string | undefined;
}

class AmqpConfig {
  @Env(['AMQP_URI'])
  AMQP_URI: string | undefined;

  @Env(['AMQP_USERNAME'])
  AMQP_USERNAME: string | undefined;

  @Env(['AMQP_PASSWORD'])
  AMQP_PASSWORD: string | undefined;

  @Env(['NODE_ENV'])
  AMQP_PREFIX: string | undefined;
}

export class AppConfig {
  @Env(['APP_NAME', 'npm_package_name'], { required: true })
  APP_NAME!: string;

  @Env(['npm_package_description'])
  APP_DESCRIPTION: string = '';

  @Env(['npm_package_version'])
  APP_VERSION: string = '';

  @Env(['LOG_LEVEL'])
  LOG_LEVEL: ILogLevel | undefined;

  @Env()
  SERVER_ENABLED: boolean = true;

  @Env()
  SERVER_PORT: number = 8080;

  @Env()
  SERVER_HOST: string = '0.0.0.0';

  @Load()
  database!: MongoConfig

  @Load()
  amqp!: AmqpConfig

  @Env(['ECHO_URL'])
  ECHO_URL: string | undefined;

  @Env(['JWT_SECRET'])
  JWT_SECRET: string | undefined;
}