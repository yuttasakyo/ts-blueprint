import * as tracer from 'jaeger-client';
import Container from 'typedi';
import { config } from './bootstrapConfig';
import { defaultLogger } from './bootstrapLogger';
import { ITracingInfo } from './libraries/tracing/interface';
import { TraceUtil } from './libraries/tracing/traceUtil';

const hpropagate = require('hpropagate');
const hpropagateTracer = require('hpropagate/lib/tracer');

const DEFAULT_TRACING_KEY = 'uber-trace-id';

const headersToPropagate = [
  'x-request-id',
  'x-b3-traceid',
  'x-b3-spanid',
  'x-b3-parentspanid',
  'x-b3-sampled',
  'x-b3-flags',
  'x-ot-span-context',
  'x-variant-id',
  'x-correlation-id',
  DEFAULT_TRACING_KEY
]

hpropagate({
  headersToPropagate,
  // setAndPropagateCorrelationId: false // disable auto generate key
})

const tracingConfig: tracer.TracingConfig = {
  serviceName: config.APP_NAME,
  sampler: {
    type: "const",
    param: 1,
  },
  reporter: {
    logSpans: true,
  },
};

const globalTracer = tracer.initTracer(tracingConfig, {
  logger: defaultLogger
});

const tracingInfo: ITracingInfo = {
  headersToPropagate,
  globalTracer,
  globalTracingKey: DEFAULT_TRACING_KEY,
  localTracer: hpropagateTracer,
  localTracingKey: 'x-correlation-id'
}

export const traceUtil = new TraceUtil(tracingInfo);

// set tracer to logger to avoid cycle dependency
defaultLogger.tracer = traceUtil;

// enable DI injection by class constructor(private traceUtil: TraceUtil)
Container.set({
  type: TraceUtil, value: traceUtil
})